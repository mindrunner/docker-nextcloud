FROM nextcloud:27

RUN apt-get update -yqq \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
  tesseract-ocr \
  tesseract-ocr-deu \
  tesseract-ocr-eng \
  gpsbabel \
  imagemagick \
  libmagickcore-6.q16-6-extra \
   && apt-get clean \
   && rm -rf /var/lib/apt/lists/*
